package com.doe.hsdirectory.application

import android.app.Application
import com.doe.hsdirectory.di.networkingModule
import com.doe.hsdirectory.di.servicesModule
import com.doe.hsdirectory.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    networkingModule,
                    servicesModule,
                    viewModelModule,
                )
            )
        }
    }
}