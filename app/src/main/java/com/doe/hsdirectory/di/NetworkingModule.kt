package com.doe.hsdirectory.di

import com.doe.hsdirectory.BuildConfig
import com.doe.hsdirectory.api.HighSchoolAPIRepository
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

const val HIGH_SCHOOL_RETROFIT = "HIGH_SCHOOL_RETROFIT"
const val REQUEST_TIMEOUT_SECONDS = 30L

@OptIn(ExperimentalSerializationApi::class)
val networkingModule = module {

    single {
        OkHttpClient
            .Builder()
            .readTimeout(REQUEST_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }

    single<Retrofit>(named(name = HIGH_SCHOOL_RETROFIT)) {
        val contentType = MediaType.get("application/json")
        val json = Json {
            ignoreUnknownKeys = true
        }
        Retrofit
            .Builder()
            .baseUrl(BuildConfig.HIGHSCHOOL_BASE_URL)
            .addConverterFactory(json.asConverterFactory(contentType))
            .client(get())
            .build()
    }

    single<HighSchoolAPIRepository> {
        get<Retrofit>(qualifier = named(name = HIGH_SCHOOL_RETROFIT))
            .create(HighSchoolAPIRepository::class.java)
    }
}