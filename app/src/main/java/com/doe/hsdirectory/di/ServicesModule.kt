package com.doe.hsdirectory.di

import com.doe.hsdirectory.api.service.highschool.HighSchoolServiceImpl
import com.doe.hsdirectory.api.service.highschool.HighSchoolService
import org.koin.dsl.module

val servicesModule = module {
    single<HighSchoolService> {
        HighSchoolServiceImpl(
            highSchoolApiRepository = get()
        )
    }

}