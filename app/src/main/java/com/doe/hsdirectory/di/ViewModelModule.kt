package com.doe.hsdirectory.di

import com.doe.hsdirectory.ui.screens.highschools.HighSchoolsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        HighSchoolsViewModel(get())
    }
}