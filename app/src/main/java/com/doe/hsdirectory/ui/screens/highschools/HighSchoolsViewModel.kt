package com.doe.hsdirectory.ui.screens.highschools

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.doe.hsdirectory.api.base.UIResponse
import com.doe.hsdirectory.api.service.highschool.HighSchoolService
import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class HighSchoolsViewModel(
    private val highSchoolService: HighSchoolService
) : ViewModel() {

    private val _viewState = MutableStateFlow<HighSchoolsViewState>(HighSchoolsViewState.Loading())

    val viewState = _viewState.asStateFlow()

    init {
        _viewState.onEach { state ->
            // monitor loading state
            // when both high schools and sat result are present then loading is complete
            if (state is HighSchoolsViewState.Loading && state.isComplete) {
                _viewState.update {
                    HighSchoolsViewState.ShowList(
                        it.highSchools,
                        it.satResults
                    )
                }
            }
        }.launchIn(
            viewModelScope
        )

        viewModelScope.launch {
            fetchHighSchoolData()
        }
    }

    private suspend fun fetchHighSchoolData() {

        // using async here so that both of the calls will be made concurrently
        val highSchoolsResponseDeferred =
            viewModelScope.async { highSchoolService.fetchHighSchools() }
        val satScoreResponseDeferred = viewModelScope.async { highSchoolService.fetchSATResults() }

        handleHighSchoolResponse(highSchoolsResponseDeferred.await())
        handleSATResponse(satScoreResponseDeferred.await())
    }

    private fun handleSATResponse(response: UIResponse<List<SATResult>>) {
        when (response) {
            is UIResponse.Error -> showErrorState()
            is UIResponse.Success -> {
                if (_viewState.value !is HighSchoolsViewState.Error) {
                    _viewState.update {
                        HighSchoolsViewState.Loading(
                            satResults = response.data,
                            highSchools = it.highSchools
                        )
                    }
                }
            }
            else -> {
                //do nothing
            }
        }
    }

    private fun handleHighSchoolResponse(response: UIResponse<List<HighSchool>>) {
        when (response) {
            is UIResponse.Error -> showErrorState()
            is UIResponse.Success -> {
                if (_viewState.value !is HighSchoolsViewState.Error) {
                    _viewState.update {
                        HighSchoolsViewState.Loading(
                            highSchools = response.data,
                            satResults = it.satResults
                        )
                    }
                }
            }
            else -> {
                //do nothing
            }
        }
    }

    private fun showErrorState() {
        _viewState.value = HighSchoolsViewState.Error
    }

    fun onSearchTextChanged(searchText: String) {
        (_viewState.value as? HighSchoolsViewState.ShowList)?.let { state ->
            _viewState.update {
                state.copy(
                    searchText = searchText
                )
            }
        }
    }

    //This function allows for re-triggering the display of the bottom sheet when
    //the same school is clicked again in the list
    fun onBottomSheetDisplayed() {
        (_viewState.value as? HighSchoolsViewState.ShowList)?.let { state ->
            _viewState.update {
                state.copy(
                    displayBottomSheet = false
                )
            }
        }
    }

    fun onHighSchoolClicked(highSchool: HighSchool) {
        (_viewState.value as? HighSchoolsViewState.ShowList)?.let { state ->
            val satResult = _viewState.value.satResults.firstOrNull {
                it.dbn == highSchool.dbn
            } ?: SATResult.NoData
            val selectedHighSchool = Pair(highSchool, satResult)

            _viewState.update {
                state.copy(
                    selectedHighSchool = selectedHighSchool,
                    displayBottomSheet = true
                )
            }
        }
    }
}