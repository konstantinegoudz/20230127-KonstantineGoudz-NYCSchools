package com.doe.hsdirectory.ui.screens.highschools.content

import androidx.activity.compose.BackHandler
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.doe.hsdirectory.R
import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult
import com.doe.hsdirectory.ui.screens.highschools.HighSchoolsViewState
import kotlinx.coroutines.launch


@Composable
@OptIn(ExperimentalMaterialApi::class)
fun SchoolDetailsBottomSheet(
    viewState: HighSchoolsViewState.ShowList,
    onBottomSheetDisplayed: () -> Unit
) {
    val bottomSheetState =
        rememberModalBottomSheetState(initialValue = ModalBottomSheetValue.Hidden)

    val scope = rememberCoroutineScope()

    if (viewState.selectedHighSchool != null && viewState.displayBottomSheet) {
        LaunchedEffect(viewState) {
            bottomSheetState.show()
            onBottomSheetDisplayed.invoke()
        }
    }

    BackHandler(enabled = bottomSheetState.isVisible) {
        scope.launch {
            bottomSheetState.hide()
        }
    }
    ModalBottomSheetLayout(
        modifier = Modifier
            .systemBarsPadding(),
        sheetBackgroundColor = MaterialTheme.colors.surface,
        sheetState = bottomSheetState,
        sheetShape = RoundedCornerShape(
            8.dp
        ),
        sheetContent = {
            viewState.selectedHighSchool?.let {
                val (highSchool, satResult) = viewState.selectedHighSchool
                HighSchoolDetails(
                    highSchool, satResult
                )
            } ?: Box(modifier = Modifier.fillMaxSize()) {
                // display an empty box since size 0 content
                //  since empty content is not allowed
            }
        }
    ) {

    }
}

@Composable
private fun HighSchoolDetails(highSchool: HighSchool, satResult: SATResult) {
    Scaffold {
        Column(modifier = Modifier.padding(it)) {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        24.dp
                    ),
                text = highSchool.schoolName,
                style = MaterialTheme.typography.h5,
                textAlign = TextAlign.Center
            )

            SATResults(satResult)
            Column(modifier = Modifier.padding(horizontal = 16.dp)) {
                Overview(highSchool)
                Address(highSchool)
                Bus(highSchool)
            }
        }
    }
}

@Composable
private fun Bus(highSchool: HighSchool) {

}

@Composable
private fun Overview(highSchool: HighSchool) {
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 24.dp, bottom = 8.dp, start = 0.dp, end = 0.dp),
        text = stringResource(R.string.overview),
        style = MaterialTheme.typography.h6
    )
    Text(
        modifier = Modifier
            .fillMaxWidth(),
        text = highSchool.overView,
    )
}

@Composable
private fun Address(highSchool: HighSchool) {
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 24.dp, bottom = 8.dp, start = 0.dp, end = 0.dp),
        text = stringResource(R.string.address),
        style = MaterialTheme.typography.h6
    )
    Row(
        modifier = Modifier.padding(vertical = 16.dp)
    ) {
        Icon(
            Icons.Rounded.LocationOn,
            null
        )
        Spacer(modifier = Modifier.padding(horizontal = 8.dp))
        with(highSchool) {
            Text(
                text = "$addressLine\n$city, $state $zip"
            )
        }
    }
}

@Composable
private fun SATResults(satResult: SATResult) {
    Card(
        modifier = Modifier.padding(
            8.dp
        ),
        backgroundColor = Color.DarkGray,
        shape = RoundedCornerShape(16.dp)
    ) {
        Column(
            modifier = Modifier.padding(
                vertical = 16.dp,
                horizontal = 8.dp
            ),
        ) {
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = stringResource(R.string.sat_scores),
                textAlign = TextAlign.Center
            )

            SATSCoreRow(R.string.num_test_takers, satResult.numOfSatTestTakers)
            SATSCoreRow(R.string.sat_score_math, satResult.satMathAvgScore)
            SATSCoreRow(R.string.sat_score_reading, satResult.satCriticalReadingAvgScore)
            SATSCoreRow(R.string.sat_score_writing, satResult.satWritingAvgScore)

        }
    }
}

@Composable
private fun SATSCoreRow(@StringRes name: Int, value: String) {
    Row(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(text = stringResource(name))
        Spacer(modifier = Modifier.weight(1f))
        Text(text = value)
    }
}
