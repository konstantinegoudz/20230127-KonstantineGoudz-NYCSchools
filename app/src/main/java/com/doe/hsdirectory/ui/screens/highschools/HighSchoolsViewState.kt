package com.doe.hsdirectory.ui.screens.highschools

import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult

sealed class HighSchoolsViewState {
    abstract val highSchools: List<HighSchool>
    abstract val satResults: List<SATResult>

    data class Loading(
        override val highSchools: List<HighSchool> = emptyList(),
        override val satResults: List<SATResult> = emptyList(),
    ) : HighSchoolsViewState() {
        val isComplete = highSchools.isNotEmpty() && satResults.isNotEmpty()
    }

    object Error : HighSchoolsViewState() {
        override val highSchools: List<HighSchool> = emptyList()
        override val satResults: List<SATResult> = emptyList()
    }

    data class ShowList(
        override val highSchools: List<HighSchool>,
        override val satResults: List<SATResult>,
        val searchText: String = "",
        val selectedHighSchool: Pair<HighSchool, SATResult>? = null,
        val displayBottomSheet: Boolean = false
    ) : HighSchoolsViewState() {

        val results: List<HighSchool>
            get() {
                return if (searchText.isNotBlank()) {
                    highSchools.filter {
                        it.schoolName
                            .lowercase()
                            .contains(
                                searchText
                                    .lowercase()
                            )
                    }
                } else {
                    highSchools
                }
            }
    }

}

