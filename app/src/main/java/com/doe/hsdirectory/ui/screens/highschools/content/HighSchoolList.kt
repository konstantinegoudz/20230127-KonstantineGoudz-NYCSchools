package com.doe.hsdirectory.ui.screens.highschools.content

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material.icons.rounded.Search
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.doe.hsdirectory.R
import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.ui.screens.highschools.HighSchoolsViewModel
import com.doe.hsdirectory.ui.screens.highschools.HighSchoolsViewState
import org.koin.androidx.compose.koinViewModel

@Composable
fun HighSchools() {
    val viewModel: HighSchoolsViewModel = koinViewModel()
    val viewState = viewModel.viewState.collectAsState()

    when (val state = viewState.value) {
        is HighSchoolsViewState.Loading -> HighSchoolsLoading()
        is HighSchoolsViewState.Error -> HighSchoolsError()
        is HighSchoolsViewState.ShowList -> HighSchoolsList(
            viewState = state,
            onSearchTextChanged = viewModel::onSearchTextChanged,
            toggleFiltering = viewModel::toggleFiltering,
            onHighSchoolClicked = viewModel::onHighSchoolClicked,
            onBottomSheetDisplayed = viewModel::onBottomSheetDisplayed,
        )
    }
}

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun HighSchoolsList(
    viewState: HighSchoolsViewState.ShowList,
    onSearchTextChanged: (String) -> Unit,
    toggleFiltering: () -> Unit,
    onHighSchoolClicked: (HighSchool) -> Unit,
    onBottomSheetDisplayed: () -> Unit,
) {

    Scaffold(
        modifier = Modifier
            .systemBarsPadding(),
        topBar = {
            SearchBar(
                viewState = viewState,
                onSearchTextChanged = onSearchTextChanged,
                toggleFiltering = toggleFiltering,
            )
        },
        backgroundColor = MaterialTheme.colors.background
    ) {
        Box(
            modifier = Modifier
                .padding(it)
                .fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .imePadding()
                    .imeNestedScroll()
            ) {
                viewState.results.forEach { highSchool ->
                    item {
                        HighSchoolItem(
                            highSchool = highSchool,
                            onHighSchoolClicked = onHighSchoolClicked
                        )
                    }
                }
            }
        }
    }
    SchoolDetailsBottomSheet(viewState, onBottomSheetDisplayed)
}

@Composable
private fun SearchBar(
    viewState: HighSchoolsViewState.ShowList,
    onSearchTextChanged: (String) -> Unit,
    toggleFiltering: () -> Unit,
) {
    TopAppBar {
        TextField(
            modifier = Modifier.weight(1f),
            value = viewState.searchText,
            onValueChange = onSearchTextChanged,
            placeholder = { Text("Search by name") },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Rounded.Search,
                    contentDescription = null
                )
            },
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            )
        )
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun HighSchoolItem(highSchool: HighSchool, onHighSchoolClicked: (HighSchool) -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp, horizontal = 8.dp)
            .clickable {
                keyboardController?.hide()
                onHighSchoolClicked.invoke(highSchool)
            }
    ) {
        Column {

            Text(
                modifier = Modifier.padding(16.dp),
                text = highSchool.schoolName,
                style = MaterialTheme.typography.h6
            )

            Divider()

            Column(
                modifier = Modifier
                    .padding(horizontal = 16.dp)
                    .padding(bottom = 16.dp),
            ) {

                Text(
                    modifier = Modifier.padding(vertical = 16.dp),
                    text = stringResource(R.string.interest, highSchool.interest)
                )

                Row {
                    Icon(
                        Icons.Rounded.LocationOn,
                        null
                    )
                    Spacer(modifier = Modifier.padding(horizontal = 8.dp))
                    Text(
                        text = "${highSchool.city}, ${highSchool.zip}"
                    )
                }
            }
        }
    }
}

@Composable
fun HighSchoolsLoading() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun HighSchoolsError() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text("Something went wrong")
    }
}