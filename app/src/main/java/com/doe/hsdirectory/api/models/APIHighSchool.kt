package com.doe.hsdirectory.api.models

import com.doe.hsdirectory.models.HighSchool
import kotlinx.serialization.SerialName

@kotlinx.serialization.Serializable
data class APIHighSchool(
    @SerialName("dbn")
    val dbn: String,
    @SerialName("school_name")
    val schoolName: String,
    @SerialName("city")
    val city: String,
    @SerialName("subway")
    val subway: String,
    @SerialName("bus")
    val bus: String,
    @SerialName("website")
    val website: String,
    @SerialName("overview_paragraph")
    val overviewParagraph: String,
    @SerialName("primary_address_line_1")
    val addressLine: String,
    @SerialName("state_code")
    val state: String,
    @SerialName("zip")
    val zipCode: String,
    @SerialName("interest1")
    val interest: String,
) {
    fun toHighSchool(): HighSchool {
        return HighSchool(
            dbn = dbn,
            schoolName = schoolName,
            city = city,
            zip = zipCode,
            subway = subway.split(","),
            bus = bus.split(","),
            website = website,
            overView = overviewParagraph,
            addressLine = addressLine,
            state = state,
            interest = interest,
        )
    }
}