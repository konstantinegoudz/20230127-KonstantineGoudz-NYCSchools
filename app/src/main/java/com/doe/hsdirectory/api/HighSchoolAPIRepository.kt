package com.doe.hsdirectory.api

import com.doe.hsdirectory.api.models.APIHighSchool
import com.doe.hsdirectory.api.models.APISATResult
import retrofit2.Response
import retrofit2.http.GET

interface HighSchoolAPIRepository {
    @GET("resource/s3k6-pzi2.json")
    suspend fun fetchHighSchools(): Response<List<APIHighSchool>>
    @GET("resource/f9bf-2cp4.json")
    suspend fun fetchSATResults(): Response<List<APISATResult>>
}