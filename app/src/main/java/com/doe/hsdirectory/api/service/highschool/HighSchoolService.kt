package com.doe.hsdirectory.api.service.highschool

import com.doe.hsdirectory.api.base.UIResponse
import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult

interface HighSchoolService {
    suspend fun fetchHighSchools(): UIResponse<List<HighSchool>>
    suspend fun fetchSATResults(): UIResponse<List<SATResult>>
}