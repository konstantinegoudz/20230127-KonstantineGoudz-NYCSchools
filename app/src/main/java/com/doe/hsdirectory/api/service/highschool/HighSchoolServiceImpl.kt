package com.doe.hsdirectory.api.service.highschool

import com.doe.hsdirectory.api.HighSchoolAPIRepository
import com.doe.hsdirectory.api.base.BaseService
import com.doe.hsdirectory.api.base.UIResponse
import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult

class HighSchoolServiceImpl(
    private val highSchoolApiRepository: HighSchoolAPIRepository
) : BaseService(), HighSchoolService {
    override suspend fun fetchHighSchools(): UIResponse<List<HighSchool>> {
        return request(
            apiRequest = { highSchoolApiRepository.fetchHighSchools() },
            transform = { result ->
                result.map {
                    it.toHighSchool()
                }
            }
        )
    }

    override suspend fun fetchSATResults(): UIResponse<List<SATResult>> {
        return request(
            apiRequest = {
                highSchoolApiRepository.fetchSATResults()
            },
            transform = { result ->
                result.map {
                    it.toSATResult()
                }
            }
        )
    }
}