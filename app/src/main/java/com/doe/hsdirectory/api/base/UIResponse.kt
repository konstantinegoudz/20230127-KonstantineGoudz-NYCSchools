package com.doe.hsdirectory.api.base

/**
 * wraps an api call in a ui friendly class which supports the loading, failed,
 * and successful states
 */
sealed class UIResponse<out T> {
    object Loading : UIResponse<Nothing>()
    data class Success<out T>(val data: T) : UIResponse<T>()
    data class Error(
        val cause: Throwable
    ) : UIResponse<Nothing>()
}