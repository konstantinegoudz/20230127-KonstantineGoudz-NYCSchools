package com.doe.hsdirectory.api.base

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

/**
 * @param [dispatcher] the coroutine dispatcher to use for api requests.
 * The default is [Dispatchers.IO] but it can be substituted for testing purposes
 */
open class BaseService(
    val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    suspend inline fun <reified APIModel, UIModel> request(
        crossinline apiRequest: suspend () -> Response<APIModel>,
        crossinline transform: (APIModel) -> UIModel
    ): UIResponse<UIModel> = withContext(dispatcher) {
        try {
            val response = apiRequest.invoke()
            val responseBody = response.body()

            if (response.isSuccessful) {
                // transform the api response to the ui model
                val uiModel = when {
                    Unit is APIModel -> transform.invoke(Unit)
                    responseBody != null -> transform.invoke(responseBody)
                    else -> throw NullPointerException("Null response body is not allowed")
                }
                UIResponse.Success(uiModel)
            } else {
                UIResponse.Error(
                    Throwable(
                        response.message()
                    )
                )
            }
        } catch (error: Throwable) {
            error.printStackTrace()
            UIResponse.Error(error)
        }
    }
}