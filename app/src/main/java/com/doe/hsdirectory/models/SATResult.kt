package com.doe.hsdirectory.models

data class SATResult(
    val dbn: String,
    val schoolName: String,
    val numOfSatTestTakers: String,
    val satCriticalReadingAvgScore: String,
    val satMathAvgScore: String,
    val satWritingAvgScore: String,
) {
    companion object {
        val NoData = SATResult(
            dbn = "",
            schoolName = "",
            numOfSatTestTakers = "",
            satMathAvgScore = "N/A",
            satCriticalReadingAvgScore = "N/A",
            satWritingAvgScore = "N/A"
        )
    }
}