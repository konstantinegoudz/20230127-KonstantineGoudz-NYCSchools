package com.doe.hsdirectory.models

data class HighSchool(
    val dbn: String = "",
    val schoolName: String = "",
    val city: String = "",
    val zip: String = "",
    val subway: List<String> = emptyList(),
    val bus: List<String> = emptyList(),
    val website: String = "",
    val overView: String = "",
    val addressLine: String = "",
    val state: String = "",
    val interest: String = ""
)