package com.doe.hsdirectory

import android.os.Bundle
import android.view.View
import android.view.WindowInsets
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.core.view.ViewCompat
import androidx.core.view.WindowCompat
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.doe.hsdirectory.ui.screens.highschools.content.HighSchools
import com.doe.hsdirectory.ui.screens.highshool.HighSchool
import com.doe.hsdirectory.ui.theme.HighSchoolDirectoryTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HighSchoolDirectoryTheme {
                val navController = rememberNavController()
                NavHost(navController = navController, startDestination = "HighSchools") {
                    composable("HighSchools") {
                        HighSchools()
                    }
                }
            }
        }

        WindowCompat.setDecorFitsSystemWindows(window, false)
    }
}




