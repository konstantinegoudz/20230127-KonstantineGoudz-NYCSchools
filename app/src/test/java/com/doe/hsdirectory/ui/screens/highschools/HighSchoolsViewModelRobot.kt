package com.doe.hsdirectory.ui.screens.highschools

import com.doe.hsdirectory.api.base.UIResponse
import com.doe.hsdirectory.api.service.highschool.HighSchoolService
import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.mockk

class HighSchoolsViewModelRobot {
    private val highSchoolService: HighSchoolService = mockk()
    private lateinit var viewModel: HighSchoolsViewModel

    fun build() = apply {
        viewModel = HighSchoolsViewModel(
            highSchoolService = highSchoolService
        )
    }

    fun mockHighSchoolResponse(response: UIResponse<List<HighSchool>>) = apply {
        coEvery {
            highSchoolService.fetchHighSchools()
        } returns response
    }

    fun mockSATScoreResponse(response: UIResponse<List<SATResult>>) = apply {
        coEvery {
            highSchoolService.fetchSATResults()
        } returns response
    }

    fun assertViewState(expected: HighSchoolsViewState) {
        assertThat(viewModel.viewState.value).isEqualTo(expected)
    }

    fun mockOnHighSchoolClicked(highSchool: HighSchool) = apply {
        viewModel.onHighSchoolClicked(highSchool)
    }
}