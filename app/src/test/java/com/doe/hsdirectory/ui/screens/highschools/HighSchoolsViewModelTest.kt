package com.doe.hsdirectory.ui.screens.highschools

import com.doe.hsdirectory.api.base.UIResponse
import com.doe.hsdirectory.fakes.uiHighSchool
import com.doe.hsdirectory.fakes.uiSATResult
import com.doe.hsdirectory.models.SATResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class HighSchoolsViewModelTest {

    private val testDispatcher = TestCoroutineDispatcher()

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `successful data load should show list`() {
        runTest {
            val highSchools = listOf(
                uiHighSchool
            )

            val satScores = listOf(
                SATResult.NoData
            )

            HighSchoolsViewModelRobot()
                .mockHighSchoolResponse(
                    UIResponse.Success(
                        highSchools
                    )
                )
                .mockSATScoreResponse(
                    UIResponse.Success(
                        satScores
                    )
                )
                .build()
                .assertViewState(
                    HighSchoolsViewState.ShowList(
                        highSchools = highSchools,
                        satResults = satScores
                    )
                )
        }
    }

    @Test
    fun `error state is shown when all data fails to load`() {
        runTest {
            HighSchoolsViewModelRobot()
                .mockHighSchoolResponse(
                    UIResponse.Error(Throwable())
                )
                .mockSATScoreResponse(
                    UIResponse.Error(Throwable())
                )
                .build()
                .assertViewState(
                    HighSchoolsViewState.Error
                )
        }
    }

    @Test
    fun `error state is shown when any data fails to load`() {
        runTest {
            val highSchools = listOf(
                uiHighSchool
            )

            HighSchoolsViewModelRobot()
                .mockHighSchoolResponse(
                    UIResponse.Success(
                        highSchools
                    )
                )
                .mockSATScoreResponse(
                    UIResponse.Error(Throwable())
                )
                .build()
                .assertViewState(
                    HighSchoolsViewState.Error
                )
        }
    }

    @Test
    fun `select a high school with no sat score data`() {
       val highSchool = uiHighSchool.copy()
        runTest {
            val highSchools = listOf(
                highSchool
            )

            val satScores = listOf(
                uiSATResult.copy(dbn = "123123")
            )

            val selectedSchool = Pair(
                highSchool,
                SATResult.NoData
            )

            val expectedViewState = HighSchoolsViewState.ShowList(
                highSchools = highSchools,
                satResults = satScores,
                selectedHighSchool = selectedSchool,
                displayBottomSheet = true
            )

            HighSchoolsViewModelRobot()
                .mockHighSchoolResponse(
                    UIResponse.Success(
                        highSchools
                    )
                )
                .mockSATScoreResponse(
                    UIResponse.Success(
                        satScores
                    )
                )
                .build()
                .mockOnHighSchoolClicked(highSchool)
                .assertViewState(
                    expectedViewState
                )
        }
    }
}