package com.doe.hsdirectory.fakes

import com.doe.hsdirectory.models.HighSchool
import com.doe.hsdirectory.models.SATResult

val uiHighSchool = HighSchool(
    dbn = "dbn",
    schoolName = "schoolName",
    city = "city",
    zip = "zip",
    subway = listOf("subway"),
    bus = listOf("bus"),
    website = "website",
    overView = "overView",
    addressLine = "addressLine",
    state = "state",
)

val uiSATResult = SATResult(
    dbn = "dbn",
    schoolName = "schoolName",
    numOfSatTestTakers = "numOfSatTestTakers",
    satCriticalReadingAvgScore = "satCriticalReadingAvgScore",
    satMathAvgScore = "satMathAvgScore",
    satWritingAvgScore = "satWritingAvgScore",
)