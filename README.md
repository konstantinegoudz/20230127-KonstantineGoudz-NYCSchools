## Konstantine Goudz NYC High schools info Android app
### A [video](https://gitlab.com/konstantinegoudz/20230127-KonstantineGoudz-NYCSchools/-/raw/main/readme/video.mp4?inline=true) of the app is included in /readme/video

## How to configure
- Add the following line to the `local.properties` file `urls.highschool="https://data.cityofnewyork.us/"`

## methods, libraries and, patterns used:
- Dependency injection using koin
- View state updates using Flows
- Ui created using Jetpack Compose
- Serialization using Kotlin serialization

## Testing
- Testing using the robot pattern
- Using mockk for creating test mocks
- Using Truth for assertions


[video](https://gitlab.com/konstantinegoudz/20230127-KonstantineGoudz-NYCSchools/-/raw/main/readme/video.mp4?inline=true)

<figure class="video_container">
  <video controls="true" allowfullscreen="true">
    <source src="https://gitlab.com/konstantinegoudz/20230127-KonstantineGoudz-NYCSchools/-/raw/main/readme/video.mp4?inline=true" type="video/mp4">
  </video>
</figure>